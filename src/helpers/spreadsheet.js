import config from "../config";

/**
 * Load the cars from the spreadsheet
 * Get the right values from it and assign.
 */
export function load(callback) {
  window.gapi.client.load("sheets", "v4", () => {
    window.gapi.client.sheets.spreadsheets.values
      .get({
        spreadsheetId: config.spreadsheetId,
        range: "Sheet0!A2:F"
      })
      .then(
        response => {
          const data = response.result.values;

          const cars = data.map(car => ({
            title: car[0],
            link: car[1],
            location: car[2],
            department: car[3],
            area: car[4],
            deadline: car[5]
          })) || [];
          callback({
            cars
          });
        },
        response => {
          callback(false, response.result.error);
        }
      );
  });
}