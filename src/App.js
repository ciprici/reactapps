import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CarList from "./components/CarList";
import Griddle from 'griddle-react';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-headerw">
          <h1 className="App-title">Job List </h1>
        </header>
        <CarList/>
      </div>
    );
  }
}

export default App;
